'use strict';

////////////////////////////////////////////////////////////////////////////////
// Model
//
const COUNTER_MAX = 10;

export const model = {
    counter: COUNTER_MAX,
    started: false,
    launched: false,
    aborted: false,

    present: data => {
        if (state.counting(model)) {
            if (model.counter === 0) {
                model.launched = data.launched || false;
            }
            else {
                model.aborted = data.aborted || false;
                if (data.counter !== undefined) {
                    model.counter = data.counter;
                }
            }
        }
        else {
            if (state.ready(model)) {
                model.started = data.started || false;
            }
        }
        state.render(model);
    }
};

////////////////////////////////////////////////////////////////////////////////
// Actions
//

const actions = {
    start: (data, present) => {
        present = present || model.present;
        data.started = true;
        present(data);
        return false;
    },

    decrement: (data, present) => {
        present = present || model.present;
        data = data || {};
        data.counter = data.counter || 10;

        setTimeout(function() {
            data.counter = data.counter - 1;
            present(data);
        }, 1000);
    },

    launch: (data, present) => {
        present = present || model.present;
        data.launched = true;
        present(data);
    },

    abort: (data, present) => {
        present = present || model.present;
        data.aborted = true;
        present(data);
        return false;
    }
};


////////////////////////////////////////////////////////////////////////////////
// View
//
export const view = {

    // Initial State
    init: model => view.ready(model),

    // State representation of the ready state
    ready: model => {
        return (
            `<p>Counter:${model.counter}</p>
            <input type="button" value="Start 4" onclick="return actions.start({});">`
        );
    },

    // State representation of the counting state
    counting: model => {
        return (
            `<p>Count down:${model.counter}</p>
			 <form onSubmit="return actions.abort({});">
				<input type="submit" value="Abort">
			</form>`
        );
    },

    // State representation of the aborted state
    aborted: model => {
        return (
            `<p>Aborted at Counter:${model.counter}</p>`
        );
    },

    // State representation of the launched state
    launched: model => {
        return (
            `<p>Launched</p>`
        );
    },

    //display the state representation
    display: representation => {
        const stateRepresentation = document.getElementById("representation");
        stateRepresentation.innerHTML = representation;
    }
};





////////////////////////////////////////////////////////////////////////////////
// State
//
export const state =  {
    ready: model => ((model.counter === COUNTER_MAX) && !model.started && !model.launched && !model.aborted),

    counting: model => ((model.counter <= COUNTER_MAX) && (model.counter >= 0) && model.started && !model.launched && !model.aborted),

    launched: model => ((model.counter == 0) && model.started && model.launched && !model.aborted),

    aborted: model => ((  model.counter <= COUNTER_MAX) && (model.counter >= 0) && model.started && !model.launched && model.aborted ),

    // Derive the state representation as a function of the systen
    // control state
    representation: model => {
        let representation = 'oops... something went wrong, the system is in an invalid state';

        if (state.ready(model)) {
            representation = view.ready(model);
        }

        if (state.counting(model)) {
            representation = view.counting(model);
        }

        if (state.launched(model)) {
            representation = view.launched(model);
        }

        if (state.aborted(model)) {
            representation = view.aborted(model);
        }

        view.display(representation);
    },

    // Next action predicate, derives whether
    // the system is in a (control) state where
    // an action needs to be invoked
    nextAction: model => {
        if (state.counting(model)) {
            if (model.counter>0) {
                actions.decrement({counter: model.counter},model.present);
            }

            if (model.counter === 0) {
                actions.launch({},model.present);
            }
        }
    },

    render: model => {
        state.representation(model)
        state.nextAction(model);
    }
};


